class_names = [
    "pratimai",
    "pasaulis",
    "spreading_publicly",
    "health_expo",
    "around_the_world",
    "exercise_pictures",
    "attacks_thuggery",
]
let slideIndex = {};
class_names.forEach((class_name) => {slideIndex[class_name] = 1})
setTimeout(() => {
    class_names.forEach((class_name) => {try { showSlides(1, class_name) } catch(err) {}})
}, 200)

// Next/previous controls
function plusSlides(n, class_name) {
  showSlides(slideIndex[class_name] += n, class_name);
}

// Thumbnail image controls
function currentSlide(n, class_name) {
  showSlides(slideIndex[class_name] = n, class_name);
}

function showSlides(n, class_name) {
  let i;
  let slides = document.getElementsByClassName(class_name);
    console.log(class_name, slides)
  let dots = document.getElementsByClassName(class_name + "_dot");
  if (n > slides.length) {slideIndex[class_name] = 1}
  if (n < 1) {slideIndex[class_name] = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex[class_name]-1].style.display = "block";
  dots[slideIndex[class_name]-1].className += " active";
} 
