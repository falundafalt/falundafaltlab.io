from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

@app.get("/")
async def root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/kas_tai")
async def root(request: Request):
    return templates.TemplateResponse("kas_tai.html", {"request": request})

@app.get("/persekiojimas")
async def root(request: Request):
    return templates.TemplateResponse("persekiojimas.html", {"request": request})

@app.get("/irodymai")
async def root(request: Request):
    return templates.TemplateResponse("irodymai.html", {"request": request})

@app.get("/istorija")
async def root(request: Request):
    return templates.TemplateResponse("istorija.html", {"request": request})

